
require('dotenv').config({path: '../../.env' })
const options = {
    'method': '',
    'url': '',
    'headers': { },
};

async function getApiData(callback){
    try{
        const reqProm = require('request-promise');
        options.url = process.env.API;
        options.method = 'GET';
        const data = await reqProm(options);
        callback(null, data);
    }
    catch(e){
        callback(null,null);
    }
}

module.exports = {
    getApiData
}