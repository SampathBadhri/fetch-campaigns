const express = require('express');
const router = express.Router();
const extApiController = require('./ext-api.controller');
const resp = require('../helpers/responseHelpers');

router.get('/externalAPI', (req, res)=>{
    try{
        extApiController.getCampigns((err, data) => {
            if(err){
                resp.errorResponse(res, err, 501, 'Error While fetching campigns');
            }else if(data){
                resp.successGetResponse(res, data, 'Campigns List');
            }else{
                resp.noRecordsFound(res, 'No slots found');
            }
        })
    }
    catch(e){
        resp.errorResponse(res, err, 501, 'Error While hitting the api');
    }
})



module.exports = router;