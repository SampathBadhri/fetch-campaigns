const extApiModel = require('./ext-api.model');
const commonFunc = require('../common-file');

async function getCampigns(callback){
    try{
        extApiModel.getApiData((err, data) => {
            if(err){
                callback(null, null);
            }else{
                let campigns,activeCamps, closedCamps;
                data = JSON.parse(data);

                //filtering Campigns
                campigns = data.map( e =>{ return {'title': e.title, 'totalAmount': parseInt(e.totalAmount), 'backersCount': e.backersCount, 'endDate': e.endDate} });

                //sorting Campigns
                campigns.sort((a, b) => (a.totalAmount > b.totalAmount ? -1 : 1));

                //fetching Active Campigns
                let presentDate = commonFunc.getDate();
                let priorDate = commonFunc.getSpecificDate(30);
                activeCamps = data.filter(e => commonFunc.getDate(e.endDate) >= presentDate && commonFunc.getDate(e.created) >= priorDate);

                //fetching closed Campigns
                closedCamps = data.filter(e => commonFunc.getDate(e.endDate) < presentDate || e.procuredAmount >= e.totalAmount);

                callback(null, {campigns : campigns, activeCamps : activeCamps, closedCamps: closedCamps});
            }
        })
    }
    catch(e){
        console.log("getCampigns Controller\n",e);
        callback(null, null);
    }
}

module.exports = {
    getCampigns
}