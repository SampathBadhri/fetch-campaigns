function successGetResponse(res, data, msg) {
    res.status(200).json({
        success: true,
        message: msg || 'Success',
        result: data
    });
}

function noRecordsFound(res, msg) {
    res.status(200).json({
        success: false,
        message: msg || 'Unable to find required information',
        result: null
    });
}

function errorResponse(res, err, status, msg) {
    res.status(status || 501).json({
        success: false,
        message: msg || 'Server Error, Please Try Again Later',
        devInfo: err
    });
}


module.exports = {
    successGetResponse,
    noRecordsFound,
    errorResponse
}