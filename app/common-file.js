let getDate = (val) => {
    const date = val ? new Date(val) : new Date();
    return date.setHours(0, 0, 0, 0);
}

let getSpecificDate = (count) => {
    const a = new Date().getDate() - count
    const b = new Date().setDate(a);
    const date = new Date(b).setHours(0,0,0,0);
    return date;
}

module.exports = {
    getDate,
    getSpecificDate
}