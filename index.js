
require('dotenv').config({ path: '.env' });
const cors = require('cors');
const bodyParser = require('body-parser');
const express = require('express');
const port = process.env.PORT || 3000;
let path = require('path');


const app = express();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Credentials', true);
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.set("view engine", "ejs");

app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.raw({ limit: '50mb', extended: true }));

app.use(express.urlencoded({ extended: false }));
app.use(cors());



const extAPI = require('./app/modules/ext-api.routes');

app.get('/', (req, res) => res.json({message: 'Welcome to DonateKart'}));

app.use('/externalApi', extAPI);


app.listen(port, () => {
    console.log(`Server is running on Port: ${port}`);
});